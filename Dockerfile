ARG FROM=build-environment:latest

# ------------------------------------------------------------------------------
# Pull base image
FROM ${FROM}

# ------------------------------------------------------------------------------
# Install tools via apt
RUN export DEBIAN_FRONTEND=noninteractive \
    && set -x \
    && apt-get -y update \
    # Install add-apt-repository
    && apt-get -y install \
        software-properties-common \
    # Setup git apt-repository
    # https://git-scm.com/download/linux
    && add-apt-repository ppa:git-core/ppa \
    && apt-get -y update \
    # Setup git-lfs apt-repository
    # https://github.com/git-lfs/git-lfs/blob/main/INSTALLING.md
    && curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash \
    && apt-get -y install \
        autoconf \
        bash-completion \
        bc \
        bison \
        build-essential \
        chrpath \
        cpio \
        diffstat \
        dos2unix \
        expect \
        fakeroot \
        flex \
        gawk \
        gcc \
        gcc-multilib \
        git \
        git-lfs \
        gnupg \
        gzip \
        iproute2 \
        less \
        libglib2.0-dev \
        libgtk2.0-0 \
        libgtk2.0-dev \
        libncurses5-dev \
        libsdl1.2-dev \
        libselinux1 \
        libssl-dev \
        libtinfo5 \
        libtool \
        libtool-bin \
        locales \
        lsb-release \
        make \
        nano \
        net-tools \
        pax \
        python3-gi \
        python3 \
        rsync \
        screen \
        socat \
        software-properties-common \
        sudo \
        tar \
        texinfo \
        tftpd-hpa \
        tofrodos \
        unzip \
        update-inetd \
        vim \
        wget \
        xorg \
        xterm \
        xvfb \
        zlib1g-dev \
    && update-alternatives --install /usr/bin/python python /usr/bin/python3 1 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists

# ------------------------------------------------------------------------------
# Install git-repo
# https://github.com/GerritCodeReview/git-repo
RUN set -x \
    && curl https://raw.githubusercontent.com/GerritCodeReview/git-repo/stable/repo > /usr/bin/repo \
    && curl https://raw.githubusercontent.com/GerritCodeReview/git-repo/stable/completion.bash > /etc/bash_completion.d/git-repo \
    && chmod +rx /usr/bin/repo

# set bash as default shell
RUN echo "dash dash/sh boolean false" | debconf-set-selections \
    && DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash

# allow sudo without password
RUN echo "%sudo ALL=(ALL:ALL) ALL" >> /etc/sudoers \
  && echo "%sudo ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

# Add user 'petalinux' and give it access to install directory /opt
RUN  set -x \
    && useradd --create-home --groups=sudo,dialout --shell=/bin/bash petalinux  \
    && chmod +w /opt \
    && chown -R petalinux:petalinux /opt

ARG XILINX_VERSION=2023.1
ARG PETALINUX_BASE=petalinux-v${XILINX_VERSION}
ARG PETALINUX_DEST=/opt/petalinux
RUN mkdir ${PETALINUX_DEST} \
    && chmod 755 ${PETALINUX_DEST} \
    && chown petalinux:petalinux ${PETALINUX_DEST}

# Install under /opt, with user petalinux
WORKDIR /opt
USER petalinux

# The PetaLinux runnable installer
ARG PETALINUX_INSTALLER=${PETALINUX_BASE}-final-installer.run
COPY --chmod=744 --chown=petalinux:petalinux resources/${PETALINUX_INSTALLER} petalinux-installer.run
RUN SKIP_LICENSE=y ./petalinux-installer.run --dir ${PETALINUX_DEST} \
    && rm -f petalinux-installer.run \
    && rm -f petalinux_installation_log

# ------------------------------------------------------------------------------
USER root
WORKDIR /root
COPY resources/.bashrc .

# ------------------------------------------------------------------------------
# Display, check and save environment settings
COPY resources/tools_version.sh /tmp/tools_version.sh
RUN set -e \
    && /tmp/tools_version.sh | tee env_settings \
    && rm /tmp/tools_version.sh

CMD [ "/bin/bash" ]
