DIR_PATH := $(realpath $(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
DOCKER_REGISTRY := registry.gitlab.com/build-environment
DOCKER_IMAGE_NAME = $(DOCKER_REGISTRY)/petalinux-env
PARENT_IMAGE_NAME = $(DOCKER_REGISTRY)/build-environment
PARENT_DOCKER_IMAGE = $(PARENT_IMAGE_NAME):latest
BUILD_DIR ?= $(DIR_PATH)/build
DEPS = Makefile Dockerfile resources/*

$(BUILD_DIR)/$(PARENT_IMAGE_NAME):
	@make -C $(DIR_PATH)/build-environment BUILD_DIR=$(BUILD_DIR)

$(BUILD_DIR):
	@mkdir -p $@
	@echo '*' > $@/.gitignore

$(BUILD_DIR)/$(DOCKER_IMAGE_NAME): $(BUILD_DIR) $(addprefix $(DIR_PATH)/, $(DEPS)) $(BUILD_DIR)/$(PARENT_IMAGE_NAME)
	@mkdir -p $(dir $@)
	@docker build --build-arg FROM=$(PARENT_DOCKER_IMAGE)  --tag $(DOCKER_IMAGE_NAME) "$(DIR_PATH)" --progress=plain 2>&1 | tee "$@.log"
	@docker images "$(DOCKER_IMAGE_NAME)" --no-trunc --quiet > $@

$(BUILD_DIR)/$(DOCKER_IMAGE_NAME).tar.gz: $(BUILD_DIR)/$(DOCKER_IMAGE_NAME)
	docker image save -o $@ $(DOCKER_IMAGE_NAME):latest

build: $(BUILD_DIR)/$(DOCKER_IMAGE_NAME)

tarball: $(BUILD_DIR)/$(DOCKER_IMAGE_NAME).tar.gz

deploy: build
	docker push $(DOCKER_IMAGE_NAME)

clean:
	docker rmi $(DOCKER_IMAGE_NAME)
	rm -rf $(BUILD_DIR)

all: build

.PHONY : all build deploy clean
.DEFAULT_GOAL := all
