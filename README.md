# petalinux-env

[![license](https://img.shields.io/gitlab/license/build-environment/petalinux-env)](https://gitlab.com/build-environment/petalinux-env)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)

`petalinux-env` is a docker image based on
[build-environment](https://gitlab.com/build-environment/build-environment)
which provides all tools to build a PetaLinnux project.

This `petalinux-env` docker image contains all tools required by
[PetaLinux Tools Documentation: Reference Guide](https://docs.xilinx.com/r/en-US/ug1144-petalinux-tools-reference-guide/Setting-Up-Your-Environment)
manual.

## How-to build the docker image

### Download petalinux package

Please download
[petalinux-v2023.1-final-installer.run](https://www.xilinx.com/member/forms/download/xef.html?filename=petalinux-v2023.1-05012318-installer.run)
inside `resources` directory before starting.

### Build the image

To build the image, use the Makefile:

```shell
$ make build
...
```

## How-to launch the docker image

The `petalinux-env` docker image can be started using `petalinux-env` script:

```shell
$ ./petalinux-env [... "docker run additional arguments"] \
> [-c ... "command executed in container"]
...
```

## How-to contribute

To contribute to this project, please ensure that the following tools are installed:

* gitlint
* pre-commit
* shfmt
* shellcheck

You can automatically check and install the tools using the following command:

```shell
$ ./dev-env-setup.sh
Start of dev environment setup
Checking tools...
* pip... OK
* shfmt... OK
* shellcheck... Installed
* pre-commit... OK
* gitlint... OK

Checking hooks...
* pre-commit... Installed
* commit-msg... OK

Dev environment is ready!
```
