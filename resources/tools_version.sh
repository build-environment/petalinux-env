#!/bin/bash

set -e -o pipefail

git --version | sed 's/version //'
git-lfs --version | sed 's/\/\([0-9\.]\+\) (.\+)/ \1/'
repo --version | grep launcher | sed 's/version //'
